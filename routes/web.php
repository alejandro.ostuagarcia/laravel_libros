<?php

use App\Http\Controllers\InicioController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\AutorController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//HOME, PAGINA DE INCIO
Route::get('/', [InicioController::class,"home"])->name('home');

//LIBROS
Route::get('libros',[LibroController::class,'index'])->name('libros.index');
Route::get('libros/create', [LibroController::class,"create"])->name('libros.create')->middleware('auth');
Route::get('libros/cat/{categoria}',[LibroController::class,'ordenar'])->name('libros.ordenar');
Route::post('libros/bucador',[LibroController::class,'buscar'])->name("libros.buscar");
Route::get('libros/{libro}',[LibroController::class,'show'])->name('libros.show');
Route::get('libros/{libro}/edit', [LibroController::class,"edit"])->name('libros.edit')->middleware('auth');

Route::put('libros/{libro}/update',[LibroController::class,"update"])->name('libros.update');
Route::post('libros',[LibroController::class,"store"])->name('libros.store');
//AUTORES
Route::get('autores',[AutorController::class,'index'])->name('autores.index');
Route::get('autores/create', [AutorController::class,"create"])->name('autores.create')->middleware('auth');
Route::get('autores/{autor}/edit', [AutorController::class,"edit"])->name('autores.edit')->middleware('auth');
Route::put('autores/{autor}',[AutorController::class,"update"])->name('autor.update');
Route::post('autores',[AutorController::class,"store"])->name('autor.store');
//LOGIN
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

