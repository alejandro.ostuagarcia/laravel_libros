# Libros Laravel

## Pagina Home
Nada mas entrar a la pagina nos encontramos la portada, si hacemos scroll hacia abajo podremos ver recomendaciones de libros y autores

## Pagina Libros(index)

Si accedemos a al enlace de la barra de navegacion "Libros" nos muestra todos los libros:


Una vez dentro vemos los libros, si ponemos el raton encima nos mostrara la informacion del libro y si clicamos nos levara a dicho libro especifico.

En la parte izquierda se muestra las categorias, esta caja tiene su propio scroll para que puedas verlas todas sin necesidad de la bajar hasta abajo de la pagina. Debajo nos muestra todos los libros, autores y categorias que hay.
## Pagina Libro(show)
Nos muestra en detalle el libro con su respectivo autor y la informacion de este

## Paginas Autores(index)
Nos muestra una lista e todos los autores y sus libros.

## Login
Podremos logearnos y añadir, modificar libros y autores, desde las paginas correspondientes.

# Controladores
## Inicio Controller
Unico metodo Home, no lleva a la pagina inicial.

## Libro Controller
En el se incluyen los metodos index,show,ordenar,buscar,create,edit,store,update para libros.

## Autor Controller 
En el se incluyen los metodos basicos index,create,edit,store,update para autores.
