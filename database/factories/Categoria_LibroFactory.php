<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\Categoria_Libro;
use App\Models\Libro;
use Illuminate\Database\Eloquent\Factories\Factory;

class Categoria_LibroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Categoria_Libro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'categoria_id'=>Categoria::all()->random()->id,
            'libro_id'=>Libro::all()->random()->id
        ];
    }
}
