<?php

namespace Database\Factories;

use App\Models\Autor;
use App\Models\Categoria;
use App\Models\Libro;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LibroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Libro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titulo = $this->faker->sentence($nbWords = 3, $variableNbWords = true);
        return [
            'titulo' =>$titulo,
            'slug'=>Str::slug($titulo),
            'autor_id'=> Autor::all()->random()->id,
            'sinopsis' =>$this->faker->paragraph($nbSentences = 9, $variableNbSentences = true),
            'precio' =>$this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
            'imagen' =>$this->faker->imageUrl($width=165, $height=250),

        ];
    }
}
