<?php

namespace Database\Factories;

use App\Models\Autor;
use Illuminate\Database\Eloquent\Factories\Factory;

class AutorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Autor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name,
            'imagen' => $this->faker->imageUrl($width=150, $height=200),
            'fecha_nacimiento'=>$this->faker->date($format = 'Y-m-d', $max = 'now'),
            'fecha_muerte'=>$this->faker->date($format = 'Y-m-d', $max = 'now'),

        ];
    }
}
