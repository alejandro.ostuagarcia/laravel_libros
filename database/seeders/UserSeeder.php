<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use phpDocumentor\Reflection\Types\This;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $usuarios =[["nombre"=>"alejandro","email"=>"alejandro.ostuagarcia@iesmiguelherrero.com","contraseña"=>"alejandro"]];
    public function run()
    {
        foreach ($this->usuarios as $usuario)
        $user = new User();
        $user->name=$usuario["nombre"];
        $user->email=$usuario["email"];
        $user->password=bcrypt($usuario["contraseña"]);
        $user->save();
    }
}
