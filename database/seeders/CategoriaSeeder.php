<?php

namespace Database\Seeders;

use App\Models\Categoria;
use App\Models\Libro;
use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $categorias =["
    Accion","Ajedrez","América","Antropología","Arte","Autoayuda","Autobiográfico","Aventuras","Bélico","Biografía","Ciencia",
        "Ciencia Ficción","Ciencias Exactas","Ciencias Naturales","Ciencias Sociales","CineClásico","Cocina",
        "Comunicación","Crítica y teoría literaria","Crónica","Cuentos","Cultura","Deportes y juegos",
        "Derecho público","Desarrollo Personal","Diccionarios y enciclopedias","Didáctico",
        "Divulgación","Divulgación científica","Drama","Economía","Educación","Ensayo","Erótico","Esoterismo",
        "España","Espiritualidad","Ética","Fantasía","Fantástico","Feminismo","Ficción","Ficción Contemporánea",
        "Ficción Histórica","Filosofía","Filosófico","Física","Guerra","Guion","Hechos Reales","Historia","Histórico",
        "Hogar","Humor","Idiomas","Infantil","Infantil y juvenil","Informática","Interactivo","Intriga","Japón",
        "Juvenil","LGTB","Literatura","Literatura fantastica","Manuales y cursos","Matemáticas",
        "Medicina","Memorias","México","Misterio","Mística","Mitología","Música","Narrativa","Narrativa contemporánea","Narrativa española",
        "Navidad","No Ficción","Novela","Novela Contemporánea","Novela del Oeste","Novela Negra","Nuclear",
        "Nutrición","Periodismo","Poesía","Policíaca","Política","Psicología","Psicológico","Publicaciónes periódicas
        ","Sátira","Sexualidad","Sociales","Sociología","Thriller","Viajes"];
    public function run()
    {
        foreach ($this->categorias as $categoria){
            $c=new Categoria();
            $c->categoria = $categoria;
            $c->save();
//            $c->Libros()->attach([
//                Libro::all()->random()->id,
//                Libro::all()->random()->id,
//                Libro::all()->random()->id,
//            ]);
        }
    }
}
