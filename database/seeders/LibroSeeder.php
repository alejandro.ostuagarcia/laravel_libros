<?php

namespace Database\Seeders;

use App\Models\Autor;
use App\Models\Categoria;
use App\Models\Libro;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LibroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $libros=["titulo"=>"El Nombre del Viento","sinopsis"=>"He robado princesas a reyes agónicos. Incendié
    la ciudad de Trebon. He pasado la noche con Felurian y he despertado vivo y cuerdo. Me expulsaron de la Universidad
    a una edad a la que a la mayoría todavía no los dejan entrar. He recorrido de noche caminos de los que otros no se
    atreven a hablar ni siquiera de día. He hablado con dioses, he amado a mujeres y he escrito canciones que hacen llorar
     a los bardos. Me llamo Kvothe. Quizá hayas oído hablar de mí.","precio"=>20,"imagen"=>"nombreDelViento.jpg"
        ];
    public function run()
    {
        $libro=$this->libros;
            $l = new Libro();
            $l->titulo= $libro["titulo"];
            $l->slug=Str::slug($libro['titulo']);
            $l->sinopsis= $libro["sinopsis"];
            $l->precio= $libro["precio"];
            $l->imagen= $libro["imagen"];
            $l->autor_id= Autor::all()->random()->id;
            $l ->save();
            $l->categorias()->attach([
                Categoria::all()->random()->id
            ]);

    }
}
