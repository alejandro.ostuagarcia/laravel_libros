<?php

namespace Database\Seeders;

use App\Models\Autor;
use Illuminate\Database\Seeder;

class AutorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $autores = ["nombre"=>"Patrick Rothfuss","imagen"=>"autor.jpg","fecha_nacimiento"=>"1973-05-06"];
    public function run()
    {
        $autor = $this->autores;
            $a = new Autor();
            $a->nombre = $autor["nombre"];
            $a->imagen = $autor["imagen"];
            $a->fecha_nacimiento = $autor["fecha_nacimiento"];
            $a->save();

    }
}
