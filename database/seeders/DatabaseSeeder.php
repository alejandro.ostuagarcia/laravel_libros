<?php

namespace Database\Seeders;

use App\Models\Autor;
use App\Models\Categoria;
use App\Models\Categoria_libro;
use App\Models\Libro;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        //Libro::factory(50)->create();
        $this->call(AutorSeeder::class);
        $this->call(CategoriaSeeder::class);
        $this->call(LibroSeeder::class);
        $this->call(UserSeeder::class);
        Autor::factory(40)->create();
        Libro::factory(50)->create();
        Categoria_libro::factory(100)->create();

    }
}
