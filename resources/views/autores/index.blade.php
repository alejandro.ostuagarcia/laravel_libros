<link rel="stylesheet" href="{{url('/assets/css/navbar.css')}}">
<link rel="stylesheet" href="{{url('/assets/css/estiloCategoria.css')}}">
@extends('layouts.master')
@section('titulo')
    Libros
@endsection
@section('contenido')
    <script>
        function cambio() {

            window.location= "{{route('libros.index')}}";
            //No he conseguido meter la variable $libro con el valor del option
            {{--window.location= "{{route('libros.show',$libro)}}";--}}
        }
    </script>
    <div class="row h-auto d-flex ms-5 ">
        <div class="col-2
            @if(Auth::check())
            border-end border-3 ms-5 ">
            <p class="mb-3 mt-5 btn btn-warning"><a class="text-decoration-none text-dark fs-1" href="{{route('autores.create')}}">Añadir autor</a></p>
            @else ms-5">
            @endif
        </div>
        <div class="col-9">
            <div class="row justify-content-center">
                @foreach($autores as $autor)
                    <div class="col-2 col-sm-3 m-5">
                        <div class="card border-0 shadow  rounded">
                                <div class="row border border-2 rounded">
                                    <img src="
                                     @if(strpos($autor->imagen,".jpg")==true)
                                    {{asset('assets/imagenes/')}}/{{$autor->imagen}}
                                    @else
                                    {{url($autor->imagen)}}
                                    @endif
                                        " class="img-fluid rounded-circle" alt="foto autor">
                                    <h2>{{$autor->nombre}}</h2>
                                    <p class="fs-3">Nacimiento: {{$autor->fecha_nacimiento}}</p>
                                    @if($autor->fecha_muerte != null)
                                    <p class="fs-3">Muerte: {{$autor->fecha_muerte}}</p>
                                    @else <p class="fs-3">Actualmente vivo</p>
                                    @endif
                                    @if(count($autor->libros)==0)

                                    @else
                                    <select onchange="cambio()" class="form-control">
                                        <option value="null">Libros:</option>
                                    @foreach($autor->libros as $libro)
                                            <option value="{{$libro}}">{{$libro->titulo}}</option>
                                    @endforeach
                                        </select>
                                    @endif
                                    @if(Auth::check())
                                    <button class=" p-0 btn btn-warning"><a class="text-decoration-none p-0 m-0 text-dark fs-1" href="{{route('autores.edit',$autor)}}">Editar autor</a></button>
                                    @endif
                                </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection



