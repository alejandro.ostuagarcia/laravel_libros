<link rel="stylesheet" href="{{url('/assets/css/navbar.css')}}">
@extends('layouts.master')
@section('titulo')
    Editar Autor
@endsection

@section('contenido')
    <div class="row mt-5">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Editar autor</div>
                <form action="{{route('autor.update',$autor)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body" style="padding:30px">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" id="nombre" value="{{$autor->nombre}}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="fecha_nacimiento">Fecha de Nacimiento</label>
                            <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" value="{{$autor->fecha_nacimiento}}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="fecha_muerte">Fecha de Muerte</label>
                            <input type="date" name="fecha_muerte" id="fecha_muerte" value="{{$autor->fecha_muerte}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="imagen">Imagen</label>
                            <input type="file" name="imagen" id="imagen" class="form-control">
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" value="Editar Libro" name="añadir" id="añadir" class="btn btn-danger mt-3">
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
@endsection
