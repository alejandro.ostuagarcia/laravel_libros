<link rel="stylesheet" href="{{url('/assets/css/navbar.css')}}">
@extends('layouts.master')
@section('titulo')
    Editar Libro
@endsection

@section('contenido')
    <div class="row mt-5">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Editar Libro</div>
                <div class="card-body" style="padding:30px">
                    <form action="{{route('libros.update',$libro)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body" style="padding:30px">
                            <div class="form-group">
                                <label for="titulo">Titulo</label>
                                <input type="text" name="titulo" id="titulo" value="{{$libro->titulo}}" class="form-control" required>
                            </div>
                            <select name="autor_id" id="autor_id" required>

                                @foreach($autores as $autor)
                                    <option value="{{$autor->id}}"
                                            @if($libro->autor_id == $autor->id)
                                            selected
                                            @endif
                                    >{{$autor->nombre}}</option>

                                @endforeach
                            </select>
                            <a href="{{route('autores.create')}}" class=" text-dark">Añadir autor</a>
                            <div class="form-group">
                                <label for="categoria">Categorias (Escoge varias con ctrl)</label><br>
                                <select name="categoria[]" id="categoria" class="form-group" multiple>
                                    @foreach($categorias as $categoria)
                                        <option value="{{$categoria->id}}"
                                        @foreach($libro->categorias as $cat)
                                        @if($categoria->id == $cat->id)
                                        selected
                                        @endif
                                        @endforeach
                                        >{{$categoria->categoria}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="precio">Precio</label>
                                <input type="number" name="precio" id="precio" value="{{$libro->precio}}"
                                       class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="enlace_descarga">Enlace de Descarga</label>
                                <input type="text" name="enlace_descarga" value="{{$libro->enlace_descarga}}"
                                       id="enlace_descarga" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="sinopsis">Sinopsis</label>
                                <textarea name="sinopsis" id="sinopsis" class="form-control"
                                          rows="10">{{$libro->sinopsis}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="imagen">Imagen</label>
                                <input type="file" name="imagen" id="imagen" class="form-control">
                            </div>
                            <div class="form-group text-center">
                                <input type="submit" value="Editar Libro" name="añadir" id="añadir"
                                       class="btn btn-danger mt-3">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
