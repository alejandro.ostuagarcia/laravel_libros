<link rel="stylesheet" href="{{url('/assets/css/navbar.css')}}">
<link rel="stylesheet" href="{{url('/assets/css/estiloCategoria.css')}}">
@extends('layouts.master')
@section('titulo')
    Libros
@endsection
@section('contenido')

    <div class="row h-auto d-flex ms-5 ">
        <div  class="col-2  ms-5 border-end border-3">
            <h1 class="mb-3 mt-5">Categorias</h1>
            <div class="row " id="categorias">
            <ul class="list-group">
                @foreach($categorias as $categoria)
                    <li><a href="{{route('libros.ordenar' , $categoria)}}"
                           class="text-decoration-none">{{$categoria->categoria}}</a></li>
                @endforeach
            </ul>
            </div>

            <div class="row mt-5">
                @if(Auth::check())
                    <hr>
                <p class="mb-3 btn btn-warning"><a class="text-decoration-none text-dark fs-1" href="{{route('libros.create')}}">Añadir Libro</a></p>
                @endif
                    <hr>
                <h1>Total libros:  {{count($libros)}}</h1>
                <h1>Categorias: {{count($categorias)}}</h1>
                <h1>Autores: {{count(\App\Models\Autor::all())}}</h1>
            </div>

        </div>
        <div class="col-9">
            <div class="row justify-content-center">
                @foreach($libros as $libro)
                    <div class="col-2 col-sm-3 m-5">
                        <div class="card border-0 shadow  rounded">
                            <a href="{{route('libros.show' , $libro)}}" class="text-decoration-none text-dark">

                                <div id="oculto">
                                    <fieldset>
                                        <legend class="fs-1 text-decoration-underline">Titulo: </legend>
                                        <p class="fs-3">{{$libro->titulo}}</p>
                                    </fieldset>
                                    <fieldset>
                                        <legend class="fs-2 text-decoration-underline">Autor:  </legend>
                                        <p class="fs-3">{{$libro->nombreAut->nombre}}</p>
                                    </fieldset>
                                    <fieldset>
                                        <label class="fs-2 text-decoration-underline">Precio:  </label>
                                        @if($libro->precio == 0 || $libro->precio == null)
                                            <label class="fs-3 bg-success text-light rounded-3">GRATIS!</label>
                                        @else
                                        <label class="fs-3">{{$libro->precio}}</label>
                                        @endif
                                    </fieldset>

                                    <fieldset>
                                        <legend class="fs-2 text-decoration-underline">Categorias:</legend>
                                    @foreach($libro->categorias as $cat)
                                         <label class="mt-2"><span class="bg-secondary rounded text-light p-1">{{$cat->categoria}}</span></label>
                                    @endforeach

                                    </fieldset>


                                </div>
                                <img id="imagen" class="card-img-top rounded" src="
                                @if(strpos($libro->imagen,".jpg")==true)
                                {{asset('assets/imagenes/')}}/{{$libro->imagen}}
                                @else
                                {{url($libro->imagen)}}
                                @endif
                                    " alt="Card image cap">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection


