<link rel="stylesheet" href="{{url('/assets/css/navbar.css')}}">
@extends('layouts.master')
@section('titulo')
    Añadir Libro
@endsection
@section('contenido')
    <div class="row mt-5">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Añadir Libro</div>
                <form action="{{route('libros.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body" style="padding:30px">
                        <h1>Libro</h1>
                        <div class="form-group">
                            <label for="titulo">Titulo</label>
                            <input type="text" name="titulo" id="titulo" class="form-control" required>
                        </div>
                        <label for="autor_id">Autores </label>
                        <select name="autor_id" id="autor_id" class="form-control" required>
                            @foreach($autores as $autor)
                                <option value="{{$autor->id}}">{{$autor->nombre}}</option>
                            @endforeach
                        </select>
                        <a href="{{route('autores.create')}}" class=" text-dark">Añadir autor</a>
                        <div class="form-group">
                            <label for="precio">Categorias (Escoge varias con ctrl)</label><br>
                            <select name="categoria[]" id="categoria" class="form-group" multiple>
                            @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="precio">Precio</label>
                            <input type="number" name="precio" id="precio" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="enlace_descarga">Enlace de Descarga</label>
                            <input type="text" name="enlace_descarga" id="enlace_descarga" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="sinopsis">Sinopsis</label>
                            <textarea name="sinopsis" id="sinopsis" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="imagen">Imagen</label>
                            <input type="file" name="imagen" id="imagen" class="form-control" required>
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" value="Añadir Nuevo Libro" name="añadir" id="añadir" class="btn btn-danger mt-3">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
