<link rel="stylesheet" href="{{url('/assets/css/navbar.css')}}">
@extends('layouts.master')
@section('titulo')
    {{$libro->titulo}}
@endsection
@section('contenido')
    <div class="container">
        <div class="row justify-content-center mb-5 mt-5">
            <div class="col-3">
                <img src="
                   @if(strpos($libro->imagen,".jpg")==true)
                {{asset('assets/imagenes/')}}/{{$libro->imagen}}
                @else
                {{url($libro->imagen)}}
                @endif
                " height="450px" width="290px"
                     alt="imagen_libro">
            </div>
            <div class="col-4 ms-4">
                <h1 style="font-family: 'Constantia', serif;">{{$libro->titulo}}</h1>
                <br><hr><br>
                <fieldset>
                    <legend class="fs-1">Categorias:</legend>
                    <p class="fs-2 ">
                        @foreach($libro->categorias as $cat)
                            {{$cat->categoria}}
                        @endforeach
                    </p>
                </fieldset>


                </h3>
                <br><br>
                <h2> Precio: {{$libro->precio}}€</h2>
                @if(Auth::check())

                <p class="mb-3 mt-5 btn btn-warning"><a class="text-decoration-none text-dark fs-1" href="{{route('libros.edit',$libro)}}">Editar Libro</a></p>
                @endif
            </div>
            <div class="col-2 ">
                <div class="row border border-2 rounded">
                    <img src="
                        @if(strpos($libro->nombreAut->imagen,".jpg")==true)
                    {{asset('assets/imagenes/')}}/{{$libro->nombreAut->imagen}}
                    @else
                    {{url($libro->nombreAut->imagen)}}
                    @endif
                        " class="img-fluid rounded-circle" alt="foto autor">
                    <h2>{{$libro->nombreAut->nombre}}</h2>
                    <p class="fs-3">Nacimiento: {{$libro->nombreAut->fecha_nacimiento}}</p>
                    @if($libro->nombreAut->fecha_muerte != null)
                        <p class="fs-3">Muerte: {{$libro->nombreAut->fecha_muerte}}</p>
                    @else <p class="fs-3">Actualmente vivo</p>
                    @endif
                </div>

                <div class="row border border-2 rounded mt-5">
                    @if($libro->precio !=0 || $libro->precio = null)
                        <input type="submit" class="btn btn-danger fs-3" name="descarga" id="descarga" value="Compralo Ya">
                    @else <input type="submit" class="btn btn-danger fs-3" name="descarga" id="descarga" value="Descargalo Gratis">
                    @endif
                </div>

            </div>
        </div>
        <hr>
        <div class="row mt-5">

            <fieldset>
                <legend><h1>Sinopsis</h1></legend>
                <p class="fs-2">{{$libro->sinopsis}}</p>
            </fieldset>

        </div>
    </div>
@endsection
