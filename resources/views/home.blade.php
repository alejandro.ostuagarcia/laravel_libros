<link rel="stylesheet" href="{{url('/assets/css/navbarHome.css')}}">
@extends('layouts.master')
@section('titulo')
    Tienda Libros
@endsection
@section('home')

    <section class="home">
    </section>
    <div style="height: 1000px">
        <!-- just to make scrolling effect possible -->
        <h1 class="myH2">Bienvenido a MundoLibro</h1>
        <p  class="ms-5 fs-3">Aqui encontraras los mejores libros!</p>
        <h2 class="ms-5">Recomendaciones libros:</h2>
        <div class="row m-5 justify-content-around">
        @foreach($libros as $libro)
                <div class="col-2">
                    <div class="card border-0 shadow  rounded ">
                        <a href="{{route('libros.show' , $libro)}}" class="text-decoration-none text-dark">
                            <div id="oculto">
                                <fieldset>
                                    <legend class="fs-1 text-decoration-underline">Titulo: </legend>
                                    <p class="fs-3">{{$libro->titulo}}</p>
                                </fieldset>
                                <fieldset>
                                    <legend class="fs-2 text-decoration-underline">Autor:  </legend>
                                    <p class="fs-3">{{$libro->nombreAut->nombre}}</p>
                                </fieldset>
                                <fieldset>
                                    <label class="fs-2 text-decoration-underline">Precio:  </label>
                                    @if($libro->precio == 0 || $libro->precio == null)
                                        <label class="fs-3 bg-success text-light rounded-3">GRATIS!</label>
                                    @else
                                        <label class="fs-3">{{$libro->precio}}</label>
                                    @endif
                                </fieldset>
                                <fieldset>
                                    <legend class="fs-2 text-decoration-underline">Categorias:</legend>
                                    @foreach($libro->categorias as $cat)
                                        <label class="mt-2"><span class="bg-secondary rounded text-light p-1">{{$cat->categoria}}</span></label>
                                    @endforeach

                                </fieldset>
                            </div>
                            <img id="imagen" class="card-img-top rounded" src="
                                    @if(strpos($libro->imagen,".jpg")==true)
                            {{asset('assets/imagenes/')}}/{{$libro->imagen}}
                            @else
                            {{url($libro->imagen)}}
                            @endif
                                " alt="Card image cap">
                        </a>
                    </div>
                </div>
        @endforeach
        </div>
        <hr>
        <h2 class="ms-5">Autores mas leidos:</h2>
        <div class="row m-5 justify-content-around">
            @foreach($autores as $autor)
                <div class="col-2">
                    <div class="card rounded">
                        <div class="row">
                            <img src="
                                     @if(strpos($autor->imagen,".jpg")==true)
                            {{asset('assets/imagenes/')}}/{{$autor->imagen}}
                            @else
                            {{url($autor->imagen)}}
                            @endif
                                " class="img-fluid rounded-circle" alt="foto autor">
                            <h2>{{$autor->nombre}}</h2>
                            <p class="fs-3">Nacimiento: {{$autor->fecha_nacimiento}}</p>
                            @if($autor->fecha_muerte != null)
                                <p class="fs-3">Muerte: {{$autor->fecha_muerte}}</p>
                            @else <p class="fs-3">Actualmente vivo</p>
                            @endif
                            @if(count($autor->libros)==0)

                            @else
                                <label class="fs-3">Libros:</label>
                                <select class="form-control">
                                    @foreach($autor->libros as $libro)

                                        <option value="">{{$libro->titulo}}</option>
                                    @endforeach
                                </select>
                            @endif
                            @if(Auth::check())
                                <button class=" p-0 btn btn-warning"><a class="text-decoration-none p-0 m-0 text-dark fs-1" href="{{route('autores.edit',$autor)}}">Editar autor</a></button>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


    </div>
@endsection
