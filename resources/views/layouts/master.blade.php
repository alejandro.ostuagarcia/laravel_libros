<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <title>@yield('titulo')</title>
</head>
<body>
@include('layouts.partials.navbar')
@yield('home')
<div class="container-fluid">
    @yield('contenido')
</div>
@include('layouts.partials.footer')
</body>
<script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
</html>
