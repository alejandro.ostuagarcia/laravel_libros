<nav class="nav">
    <div class="container">
        <div class="logo">
            <img src="{{asset('assets/imagenes/')}}/icono.png" class="position-absolute" style="left: 6%" height="60px"
                 width="50px" alt="">
            <a href="{{route('home')}}" id="nombreTitulo" class="p-3">MundoLibro</a>
        </div>

        <div id="mainListDiv" class="main_list">
            <ul class="navlinks">
                <li>
                    <form class="mt-4" action="{{route('libros.buscar')}}" method="post">
                        @csrf
                        <input type="text" name="texto" id="buscarlibro" style="height: 35px; width: 200px" placeholder="Buscar libro">
                    </form>
                </li>
                <li><a href="{{route('libros.index')}}">Libros</a></li>
                <li><a href="{{route('autores.index')}}">Autores</a></li>
                @if(Auth::check())
                    <li>
                        <a href="{{ route('profile.show') }}">{{ Auth::user()->name }}</a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" >
                            <span class="glyphicon glyphicon-off"></span>
                            Cerrar sesión
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @else
                    <li><a href="{{url('login')}}">Login</a></li>
                @endif
            </ul>
        </div>
        <span class="navTrigger">
                <i></i>
                <i></i>
                <i></i>
        </span>

    </div>

</nav>
<!-- Jquery needed -->
<script src="{{url('/assets/jquery/jquery3.1.js')}}"></script>
<script src="js/scripts.js"></script>

<!-- Function used to shrink nav bar removing paddings and adding black background -->
<script>
    $(window).scroll(function () {
        if ($(document).scrollTop() > 50) {
            $('.nav').addClass('affix');
            console.log("OK");
        } else {
            $('.nav').removeClass('affix');
        }
    });
</script>

