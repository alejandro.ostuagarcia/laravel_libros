<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function getRouteKeyName() {
        return'slug';
    }

    public function categorias() {
        return $this->belongsToMany(Categoria::class);
    }

    public function nombreAut() {
        return $this->belongsTo(Autor::class,"autor_id");
    }
}
