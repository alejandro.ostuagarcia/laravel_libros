<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria_libro extends Model
{
    use HasFactory;
    protected $table="categoria_libro";
    public $timestamps = false;
}
