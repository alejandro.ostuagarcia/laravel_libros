<?php

namespace App\Http\Controllers;

use App\Models\Autor;
use Illuminate\Http\Request;

class AutorController extends Controller
{
    public function index()
    {
        $autores = Autor::all();
        return view('autores.index', compact('autores'));
    }

    public function edit(Autor $autor)
    {
        return view('autores.edit', compact('autor'));
    }

    public function create()
    {
        return view('autores.create');
    }

    public function store(Request $request)
    {
        $a= new Autor();
        $a->nombre = $request->nombre;
        $a->fecha_nacimiento = $request->fecha_nacimiento;
        $a->fecha_muerte = $request->fecha_muerte;
        $a->imagen = $request->imagen->store('','libros');
        $a->save();

        return redirect()->route('autores.index');
    }

    public function update(Request $request,$autor)
    {
        $a= Autor::findOrFail($autor);
        $a->nombre = $request->nombre;
        $a->fecha_nacimiento = $request->fecha_nacimiento;
        $a->fecha_muerte = $request->fecha_muerte;
        if (!empty($request->imagen))
        {
            Storage::disk('libros')->delete($a->imagen);
            $a->imagen = $request->imagen->store("",'libros');
        }
        $a->save();
        return redirect()->route('autores.index');
    }
}
