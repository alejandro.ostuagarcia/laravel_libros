<?php

namespace App\Http\Controllers;

use App\Models\Autor;
use App\Models\Libro;
use Illuminate\Http\Request;

class InicioController extends Controller
{
    public function home()
    {
        $libros = Libro::inRandomOrder()->limit(5)->get();
        $autores = Autor::inRandomOrder()->limit(5)->get();
        return view('home',["libros"=>$libros,"autores"=>$autores]);
    }
}
