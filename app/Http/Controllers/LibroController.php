<?php

namespace App\Http\Controllers;

use App\Models\Autor;
use App\Models\Categoria;
use App\Models\Categoria_libro;
use App\Models\Libro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LibroController extends Controller
{
    public function index(){
        $libros = Libro::all();
        $categorias = Categoria::all();
        return view("libros.index",["libros"=>$libros,"categorias"=>$categorias]);
    }

    public function show(Libro $libro){
        return view("libros.show",compact("libro"));
    }

    public function ordenar(Categoria $categoria){
        $categorias = Categoria::all();
        $libros =  $categoria->Libros()->where('categoria_id','=',$categoria->id)->get();
        return view("libros.index",["libros"=>$libros,"categorias"=>$categorias]);
    }

    public function buscar(Request $request) {
        $categorias = Categoria::all();
        $busqueda='%'.$request->texto.'%';
        $libros = Libro::where('titulo','like',$busqueda)->get();
        return view("libros.index",["libros"=>$libros,"categorias"=>$categorias]);
    }

    public function create()
    {
        $autores = Autor::all();
        $categorias = Categoria::all();
        return view("libros.create",["autores"=>$autores,"categorias"=>$categorias]);
    }
    public function edit(Libro $libro)
    {
        $autores = Autor::all();
        $categorias = Categoria::all();
        return view("libros.edit", ["libro"=>$libro,"autores"=>$autores,"categorias"=>$categorias]);
    }
    public function store(Request $request){

        $libro= new Libro();
        $libro->titulo = $request->titulo;
        $libro->slug = Str::slug($request->titulo);
        $libro->autor_id = $request->autor_id;
        $libro->precio = $request->precio;
        $libro->sinopsis = $request->sinopsis;
        $libro->enlace_descarga = $request->enlace_descarga;
        $libro->imagen = $request->imagen->store('','libros');
        $libro->save();

        foreach ($request->categoria as $categoria)
        {
            $categoria_libro= new Categoria_libro();
            $categoria_libro->categoria_id = $categoria;
            $categoria_libro->libro_id= $libro->id;
            $categoria_libro->save();
        }
        return redirect()->route('libros.index');
    }

    public function update(Request $request,$libro)
    {
        $l= Libro::find($libro);
//        $l = Libro::findOrFail($libro);
        $l->titulo = $request->titulo;
        $l->slug = Str::slug($request->titulo);
        $l->autor_id = $request->autor_id;
        $l->precio = $request->precio;
        $l->sinopsis = $request->sinopsis;

        $l->enlace_descarga = $request->enlace_descarga;
        if (!empty($request->imagen))
        {
            Storage::disk('libros')->delete($l->imagen);
            $l->imagen = $request->imagen->store("",'libros');
        }
        $l->save();
        return redirect()->route('libros.index');

    }
}
